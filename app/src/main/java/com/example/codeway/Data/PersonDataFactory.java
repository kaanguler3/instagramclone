package com.example.codeway.Data;

import com.example.codeway.Model.PersonModel;
import com.example.codeway.Model.Story.ImageStoryModel;
import com.example.codeway.Model.Story.StoryModel;
import com.example.codeway.Model.Story.VideoStoryModel;
import com.example.codeway.R;
import java.util.ArrayList;

public class PersonDataFactory {
    public static ArrayList<PersonModel> generateData(){
        ArrayList<PersonModel> people = new ArrayList<>();

        ArrayList<StoryModel> storiesPerson1 = new ArrayList<>();
        storiesPerson1.add(new ImageStoryModel("https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Will_Smith_by_Gage_Skidmore.jpg/1200px-Will_Smith_by_Gage_Skidmore.jpg"));
        storiesPerson1.add(new ImageStoryModel("https://i.haberglobal.com.tr/storage/haber/2020/07/14/jada-pinkett-esi-will-smith-i-aldattigini-canli-yayinda-itiraf-etti_1594719510.jpg"));
        storiesPerson1.add(new VideoStoryModel("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4"));
        storiesPerson1.add(new VideoStoryModel("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4"));
        PersonModel person1 = new PersonModel("Will",
                "https://iasbh.tmgrup.com.tr/5965aa/366/218/3/0/534/316?u=https://isbh.tmgrup.com.tr/sbh/2018/01/17/will-smith-kimdir-1516171992136.jpg",
                storiesPerson1);

        ArrayList<StoryModel> storiesPerson2 = new ArrayList<>();
        storiesPerson2.add(new ImageStoryModel("https://tr.web.img4.acsta.net/pictures/15/07/03/11/11/505564.jpg"));
        storiesPerson2.add(new ImageStoryModel("https://www.buseterim.com.tr/upload/default/2016/12/20/margotevlendi1000.jpg"));
        PersonModel person2 = new PersonModel("Margot",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Robbie_at_2019_Cannes_%28cropped%29.jpg/220px-Robbie_at_2019_Cannes_%28cropped%29.jpg",
                storiesPerson2);


        ArrayList<StoryModel> storiesPerson3 = new ArrayList<>();
        storiesPerson3.add(new ImageStoryModel("https://im.haberturk.com/2019/07/16/ver1563281846/2504822_810x458.jpg"));
        storiesPerson3.add(new VideoStoryModel("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4"));
        storiesPerson3.add(new VideoStoryModel("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"));
        storiesPerson3.add(new ImageStoryModel("https://i4.hurimg.com/i/hurriyet/75/750x422/56e701df18c773351851f609.jpg"));
        PersonModel person3 = new PersonModel("Cem",
                "https://tr.web.img2.acsta.net/c_215_290/pictures/15/05/07/13/39/405251.jpg",
                storiesPerson3);

        ArrayList<StoryModel> storiesPerson4 = new ArrayList<>();
        storiesPerson4.add(new ImageStoryModel("https://media2.s-nbcnews.com/j/streams/2012/October/121018/1C4346703-g-ent-121017-russell-crowe-noah.fit-760w.jpg"));
        storiesPerson4.add(new ImageStoryModel("https://img-s-msn-com.akamaized.net/tenant/amp/entityid/BB100Jil.img?h=0&w=600&m=6&q=60&u=t&o=f&l=f&x=862&y=645"));
        PersonModel person4 = new PersonModel("Russell",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Russell_Crowe_%2833994020424%29.jpg/1200px-Russell_Crowe_%2833994020424%29.jpg",
                storiesPerson4);

        ArrayList<StoryModel> storiesPerson5 = new ArrayList<>();
        storiesPerson5.add(new ImageStoryModel("https://gmag.com.tr/wp-content/uploads/2018/10/e-0-1.jpg"));
        storiesPerson5.add(new ImageStoryModel("https://i.sozcu.com.tr/wp-content/uploads/2019/11/12/iecrop/emma-kapak_1_1_1573538354-400x400.jpg"));
        storiesPerson5.add(new VideoStoryModel("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4"));
        PersonModel person5 = new PersonModel("Emma",
                "https://tr.web.img3.acsta.net/pictures/14/03/26/17/55/164231.jpg",
                storiesPerson5);

        ArrayList<StoryModel> storiesPerson6 = new ArrayList<>();
        storiesPerson6.add(new ImageStoryModel("https://foto.haberler.com/haber/2020/05/02/asli-enver-hastaligiyla-ilgili-gelen-soruya-yanit-13183096_8038_amp.jpg"));
        storiesPerson6.add(new ImageStoryModel("https://foto.haberler.com/haber/2020/02/01/asli-enver-den-flas-murat-boz-aciklamasi-12875608_amp.jpg"));
        PersonModel person6 = new PersonModel("Aslı", "https://i4.hurimg.com/i/hurriyet/75/750x422/59c61d5418c7732838b7cb11.jpg", storiesPerson6);

        ArrayList<StoryModel> storiesPerson7 = new ArrayList<>();
        storiesPerson7.add(new ImageStoryModel("https://i2.cnnturk.com/i/cnnturk/75/630x0/5edb7b7879da3e0f687c7443.jpg"));
        storiesPerson7.add(new ImageStoryModel("https://ajssarimg.mediatriple.net/1114655.jpg"));
        PersonModel person7 = new PersonModel("Acun",
                "https://i.sozcu.com.tr/wp-content/uploads/2019/12/20/iecrop/images-30_1_1_1576853767-400x400.jpg",
                storiesPerson7);

        ArrayList<StoryModel> storiesPerson8 = new ArrayList<>();
        storiesPerson8.add(new ImageStoryModel("https://i.sozcu.com.tr/wp-content/uploads/2020/03/31/iecrop/angelina-splash_16_9_1585663494.jpg"));
        storiesPerson8.add(new ImageStoryModel("https://icdn.ensonhaber.com/resimler/galeri/4_11952.jpg"));
        PersonModel person8 = new PersonModel("Angelina",
                "https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5ed67918c6ade40006ffd6db%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D537%26cropX2%3D1617%26cropY1%3D0%26cropY2%3D1080",
                storiesPerson8);
        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        people.add(person5);
        people.add(person6);
        people.add(person7);
        people.add(person8);
        return people;
    }
}
