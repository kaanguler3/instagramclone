package com.example.codeway.Model.Story;

import com.example.codeway.Enum.StoryType;

public class VideoStoryModel extends StoryModel{
    private boolean isStartEnabled;
    public VideoStoryModel(String url){
        super(url, StoryType.VIDEO);
    }

    public boolean isStartEnabled() {
        return isStartEnabled;
    }

    public void setStartEnabled(boolean startEnabled) {
        isStartEnabled = startEnabled;
    }
}
