package com.example.codeway.Model.Story;

import com.example.codeway.Constants;
import com.example.codeway.Enum.StoryType;

public class ImageStoryModel extends StoryModel{
    public ImageStoryModel(String url){
        super(url, StoryType.IMAGE, Constants.DURATION_IMAGE_STORY_SECONDS);
    }
}
