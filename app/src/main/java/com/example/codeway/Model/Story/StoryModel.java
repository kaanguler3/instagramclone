package com.example.codeway.Model.Story;

import com.example.codeway.Enum.StoryType;

import java.io.Serializable;

public class StoryModel implements Serializable {
    public static int lastStoryId;
    private int id;
    private int durationSeconds;
    private String url;
    private StoryType storyType;
    private boolean isWatched;

    public StoryModel(String url, StoryType storyType){
        lastStoryId++;
        this.id = lastStoryId;
        this.url = url;
        this.storyType = storyType;
    }

    public StoryModel(String url, StoryType storyType, int durationSeconds){
        lastStoryId++;
        this.id = lastStoryId;
        this.url = url;
        this.storyType = storyType;
        this.durationSeconds = durationSeconds;
    }

    public static int getLastStoryId() {
        return lastStoryId;
    }

    public static void setLastStoryId(int lastStoryId) {
        StoryModel.lastStoryId = lastStoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(int durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public StoryType getStoryType() {
        return storyType;
    }

    public void setStoryType(StoryType storyType) {
        this.storyType = storyType;
    }

    public boolean isWatched() {
        return isWatched;
    }

    public void setWatched(boolean watched) {
        isWatched = watched;
    }
}
