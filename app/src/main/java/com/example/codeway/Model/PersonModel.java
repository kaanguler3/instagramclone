package com.example.codeway.Model;

import com.example.codeway.Model.Story.StoryModel;

import java.io.Serializable;
import java.util.ArrayList;

public class PersonModel implements Serializable {
    public static int lastPersonId;
    private int id;
    private String name;
    private ArrayList<StoryModel> stories;
    private String imageUrl;
    private int currentStoryPos = 0;

    public PersonModel(String name, String imageUrl, ArrayList<StoryModel> stories){ //all person need stories to initialize
        lastPersonId++;
        this.id = lastPersonId;
        this.name = name;
        this.imageUrl = imageUrl;
        this.stories = stories;

    }

    public static int getLastPersonId() {
        return lastPersonId;
    }

    public static void setLastPersonId(int lastPersonId) {
        PersonModel.lastPersonId = lastPersonId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<StoryModel> getStories() {
        return stories;
    }

    public void setStories(ArrayList<StoryModel> stories) {
        this.stories = stories;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentStoryPos() {
        return currentStoryPos;
    }

    public void setCurrentStoryPos(int currentStoryPos) {
        this.currentStoryPos = currentStoryPos;
    }
}
