package com.example.codeway.Adapter;

import android.app.Person;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.example.codeway.Activity.StoriesActivity;
import com.example.codeway.Enum.StoryType;
import com.example.codeway.Fragment.StoryContainerFragment;
import com.example.codeway.Model.PersonModel;
import com.example.codeway.Model.Story.StoryModel;
import com.example.codeway.Model.Story.VideoStoryModel;
import com.example.codeway.R;

import java.util.ArrayList;

public class StoryProgressAdapter extends RecyclerView.Adapter<StoryProgressAdapter.ItemViewHolder> {

    private StoriesActivity activity;
    private StoryContainerFragment storyContainerFragment;
    private PersonModel person;
    public ArrayList<StoryModel> allItems;
    private ArrayList<StoryProgressAdapter.ItemViewHolder> holders;
    private boolean isInitialized;
    private boolean isPaused;
    private boolean isResume;

    public StoryProgressAdapter(StoryContainerFragment storyContainerFragment, PersonModel person) {
        this.activity = (StoriesActivity)storyContainerFragment.getActivity();
        this.storyContainerFragment = storyContainerFragment;
        this.person = person;
        this.allItems = person.getStories();
        this.holders = new ArrayList<>();
    }

    public StoryProgressAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_of_story_progress_recycler, parent, false);
        return new StoryProgressAdapter.ItemViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(StoryProgressAdapter.ItemViewHolder holder, int position) {
        StoryModel item = getItem(position);

        PersonModel currentPerson = activity.getCurrentPerson();
        StoryModel currentStory = person.getStories().get(person.getCurrentStoryPos());


        if(isPaused){
            if(holder.timer != null){
                holder.timer.cancel();
                holder.timer = null;
            }
        }
        else if (currentPerson.getId() == person.getId() && position == person.getCurrentStoryPos() && holder.timer == null) {
            if(item.getStoryType() == StoryType.VIDEO){
                VideoStoryModel videoStory = (VideoStoryModel)item;
                if(!videoStory.isStartEnabled()){
                    //video is loading didn't start yet
                    holder.progressBar.setProgress(0);
                    return;
                }
            }

            int totalMills = item.getDurationSeconds() * 1000;
            int intervalMills = totalMills / holder.progressBar.getMax();
            int timerMills = 0;
            if(isResume){
                isResume = false;
                timerMills = (int)holder.remainMills;
                holder.progressBar.setProgress(holder.lastProgress);
            }
            else {
                timerMills = totalMills;
                holder.progressBar.setProgress(0);
            }


            holder.timer = new CountDownTimer(timerMills, intervalMills) { //increase progress 1 on every tick
                public void onTick(long millisUntilFinished) {
                    //forward progress
                    holder.remainMills = millisUntilFinished;
                    int currentProgress = holder.progressBar.getProgress();
                    currentProgress++;
                    holder.lastProgress = currentProgress;
                    holder.progressBar.setProgress(currentProgress);
                }

                public void onFinish() {
                    // DO something when 1 minute is up
                    holder.timer.cancel();
                    holder.timer = null;
                    storyContainerFragment.goNextStory();
                }
            };
            holder.timer.start();
        }
        else if(isInitialized && item.getId() != currentStory.getId()){
            if(holder.timer != null){
                holder.timer.cancel();
                holder.timer = null;
            }
            if(position >= person.getCurrentStoryPos()){
                holder.progressBar.setProgress(0);
            }
            else {
                holder.progressBar.setProgress(holder.progressBar.getMax());
            }
        }
    }

    public void videoStarted(StoryModel story, int durationSeconds){
        int i=0;
        while (i< allItems.size()){
            StoryModel item = allItems.get(i);
            if(item.getId() == story.getId()){
                item.setDurationSeconds(durationSeconds);
                VideoStoryModel videoStoryModel = (VideoStoryModel)item;
                videoStoryModel.setStartEnabled(true);
                notifyDataSetChanged();
                return;
            }
            i++;
        }
    }

    public void cancelTimers(){
        for (ItemViewHolder holder: holders){
            if(holder.timer != null){
                holder.timer.cancel();
                holder.timer = null;
            }
        }
    }

    public void pause(){
        isPaused = true;
        notifyDataSetChanged();
    }

    public void resume(){
        isPaused = false;
        isResume = true;
        notifyDataSetChanged();
    }

    public void click(){
        isPaused = false;
    }

    @Override
    public int getItemCount() {
        return allItems.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        CountDownTimer timer;
        long remainMills;
        int lastProgress;
        ItemViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
            holders.add(this);
        }
    }

    public StoryModel getItem(int position) {
        return allItems.get(position);
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public void setInitialized(boolean initialized) {
        isInitialized = initialized;
    }
}
