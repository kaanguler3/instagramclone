package com.example.codeway.Adapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.codeway.Fragment.StoryContainerFragment;
import com.example.codeway.Model.PersonModel;

import java.util.ArrayList;

public class StoryContainerAdapter extends FragmentPagerAdapter {
    private ArrayList<PersonModel> allItems;
    private ArrayList<StoryContainerFragment> fragments;
    public StoryContainerAdapter(FragmentManager fragmentManager, ArrayList<PersonModel> allItems) {
        super(fragmentManager);
        this.allItems = allItems;
        this.fragments = new ArrayList<>();
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return allItems.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        PersonModel item = allItems.get(position);
        StoryContainerFragment fragment = StoryContainerFragment.newInstance(item);
        fragments.add(fragment);
        return fragment;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    public void updateProgressView(int prevPos, int position){
        if(prevPos != position){
            StoryContainerFragment fragmentPrev = fragments.get(prevPos);
            fragmentPrev.updateProgressView();
            fragmentPrev.stopVideo();
        }

        StoryContainerFragment fragment = fragments.get(position);
        fragment.updateProgressView();
        fragment.restartVideo();
    }
}
