package com.example.codeway.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.example.codeway.Interface.RecyclerItemClickListener;
import com.example.codeway.Model.PersonModel;
import com.example.codeway.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PeopleStoriesRecyclerAdapter extends RecyclerView.Adapter<PeopleStoriesRecyclerAdapter.ItemViewHolder> {
    private Context context;
    public ArrayList<PersonModel> allItems;
    public RecyclerItemClickListener listener;
    public PeopleStoriesRecyclerAdapter(Context context, ArrayList<PersonModel> allItems, RecyclerItemClickListener listener) {
        this.context = context;
        this.allItems = allItems;
        this.listener = listener;
    }

    public PeopleStoriesRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_of_person_stories_recycler, parent, false);
        return new PeopleStoriesRecyclerAdapter.ItemViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(PeopleStoriesRecyclerAdapter.ItemViewHolder holder, int position) {
        PersonModel item = getItem(position);
        Picasso.get().load(item.getImageUrl()).into(holder.imageView);
        holder.txtPersonName.setText(item.getName());
        boolean isStoriesWatched = item.getStories().get(item.getStories().size()-1).isWatched();
        if(isStoriesWatched){
            holder.circleWatchStatus.setBackgroundResource(R.drawable.background_person_image_watched);
        }
        else {
            holder.circleWatchStatus.setBackgroundResource(R.drawable.background_person_image_not_watched);
        }
    }

    @Override
    public int getItemCount() {
        return allItems.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        View itemView;
        ImageView imageView;
        TextView txtPersonName;
        View pnlImage;
        View circleWatchStatus;
        ItemViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.imageView = itemView.findViewById(R.id.imageView);
            this.txtPersonName = itemView.findViewById(R.id.txtPersonName);
            this.pnlImage = itemView.findViewById(R.id.pnlImage);
            this.circleWatchStatus = itemView.findViewById(R.id.circleWatchStatus);
            this.itemView.setOnClickListener(this);
            itemView.setTag(pnlImage);
        }

        @Override
        public void onClick(View view) {
            View pnlImage = (View)itemView.getTag();

            final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.shrink);
            pnlImage.startAnimation(myAnim);
            myAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    listener.onItemClick(view, getAdapterPosition());
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    public PersonModel getItem(int position) {
        return allItems.get(position);
    }

    public ArrayList<PersonModel> getAllItems() {
        return allItems;
    }

    public void setAllItems(ArrayList<PersonModel> allItems) {
        this.allItems = allItems;
        notifyDataSetChanged();
    }
}
