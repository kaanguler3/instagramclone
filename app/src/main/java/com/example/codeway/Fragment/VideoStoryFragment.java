package com.example.codeway.Fragment;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.fragment.app.Fragment;

import com.example.codeway.Activity.StoriesActivity;
import com.example.codeway.Constants;
import com.example.codeway.Model.PersonModel;
import com.example.codeway.Model.Story.StoryModel;
import com.example.codeway.Model.Story.VideoStoryModel;
import com.example.codeway.R;

public class VideoStoryFragment extends Fragment {
    private StoriesActivity activity;
    private StoryContainerFragment storyContainerFragment;
    private TextView txtLoading;
    private VideoView videoView;
    private VideoStoryModel story;
    private View pnlVideo;

    public static VideoStoryFragment newInstance(StoryModel story) {
        Bundle args = new Bundle();
        VideoStoryFragment fragment = new VideoStoryFragment();
        args.putSerializable(Constants.KEY_STORY, story);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        story = (VideoStoryModel) getArguments().getSerializable(Constants.KEY_STORY);
        storyContainerFragment = (StoryContainerFragment) getParentFragment();
        activity = (StoriesActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_story, container, false);
        pnlVideo = view.findViewById(R.id.pnlVideo);
        videoView = view.findViewById(R.id.videoView);
        txtLoading = view.findViewById(R.id.txtLoading);
        return view;
    }

    public void restartVideo() {
        initializeUI();
        initializePlayer();
    }

    public void stopVideo(){
        releasePlayer();
    }

    private void initializeUI() {
        videoView.setOnPreparedListener(
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        txtLoading.setVisibility(VideoView.INVISIBLE);
                        videoView.seekTo(1);

                        long durationMills = videoView.getDuration();
                        int durationSeconds = (int) (durationMills / 1000);
                        storyContainerFragment.onVideoReady(story, durationSeconds);
                    }
                });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                storyContainerFragment.onVideoFinish();
            }
        });
    }

    private void initializePlayer() {
        Uri videoUri = getMedia(story.getUrl());
        videoView.setVideoURI(videoUri);
        videoView.start();
    }

    private Uri getMedia(String mediaName) {
        if (URLUtil.isValidUrl(mediaName)) {
            // media name is an external URL
            return Uri.parse(mediaName);
        } else { // media name is a raw resource embedded in the app
            return Uri.parse("android.resource://" + getActivity().getPackageName() + "/raw/" + mediaName);
        }
    }

    private void releasePlayer() {
        videoView.stopPlayback();
    }

    public void pause(){
        videoView.pause();
    }

    public void resume(){
        videoView.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        PersonModel person = activity.getCurrentPerson();
        StoryModel story = person.getStories().get(person.getCurrentStoryPos());
        if(this.story.getId() == story.getId()){
            initializeUI();
            initializePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoView.pause();
        }
    }
}
