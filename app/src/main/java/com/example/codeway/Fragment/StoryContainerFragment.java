package com.example.codeway.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.codeway.Activity.StoriesActivity;
import com.example.codeway.Adapter.StoryProgressAdapter;
import com.example.codeway.Constants;
import com.example.codeway.Enum.StoryType;
import com.example.codeway.Model.PersonModel;
import com.example.codeway.Model.Story.StoryModel;
import com.example.codeway.Model.Story.VideoStoryModel;
import com.example.codeway.R;
import com.example.codeway.SpanningLinearLayoutManager;
import java.util.ArrayList;

public class StoryContainerFragment extends Fragment {

    private RecyclerView recyclerStoryProgress;
    private StoryProgressAdapter adapterStoryProgress;
    private View viewLeftArea;
    private View viewRightArea;
    private StoriesActivity activity;
    private FragmentManager childFragmentManger;
    private PersonModel person;
    private final int CLICK_THRESHOLD = 100;

    public static StoryContainerFragment newInstance(PersonModel person) {
        Bundle args = new Bundle();
        StoryContainerFragment fragment = new StoryContainerFragment();
        args.putSerializable(Constants.KEY_PERSON, person);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        person = (PersonModel) getArguments().getSerializable(Constants.KEY_PERSON);
        activity = (StoriesActivity) getActivity();
        childFragmentManger = getChildFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_story_container, container, false);

        recyclerStoryProgress = view.findViewById(R.id.recyclerStoryProgress);
        SpanningLinearLayoutManager layoutManager = new SpanningLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerStoryProgress.setLayoutManager(layoutManager);
        viewLeftArea = view.findViewById(R.id.viewLeftArea);
        viewRightArea = view.findViewById(R.id.viewRightArea);

        initializeUI();
        return view;
    }

    public void initializeUI(){
        viewLeftArea.setTag(0);
        viewRightArea.setTag(1);
        viewLeftArea.setOnTouchListener(OnScreenTouch());
        viewRightArea.setOnTouchListener(OnScreenTouch());

        adapterStoryProgress = new StoryProgressAdapter(this, person);
        recyclerStoryProgress.setAdapter(adapterStoryProgress);
        updateUIForNextStory();
    }

    private void updateUIForNextStory(){
        activity.setStoryWatched(person, person.getCurrentStoryPos());
        ArrayList<StoryModel> stories = person.getStories();
        StoryModel story = stories.get(person.getCurrentStoryPos());
        Fragment fragment = null;
        if(story.getStoryType() == StoryType.VIDEO){
            fragment = VideoStoryFragment.newInstance(stories.get(person.getCurrentStoryPos()));
        }
        else {
            fragment = ImageStoryFragment.newInstance(stories.get(person.getCurrentStoryPos()));
        }
        replaceFragment(fragment, story.getId());
        notifyDataSetChanged();//to update timer
    }

    private void notifyDataSetChanged(){
        if(adapterStoryProgress.isInitialized()){
            recyclerStoryProgress.post(new Runnable()
            {
                @Override
                public void run() {
                    adapterStoryProgress.notifyDataSetChanged();
                }
            });
        }
        else {
            adapterStoryProgress.setInitialized(true);
        }
    }

    public void updateProgressView(){
        ArrayList<StoryModel> stories = person.getStories();
        StoryModel story = stories.get(person.getCurrentStoryPos());
        notifyDataSetChanged();// update timer
    }

    public void stopVideo(){
        ArrayList<StoryModel> stories = person.getStories();
        StoryModel story = stories.get(person.getCurrentStoryPos());
        if(story.getStoryType() == StoryType.VIDEO){
            String tag = String.valueOf(story.getId());
            VideoStoryFragment fragment = (VideoStoryFragment)getChildFragmentManager().findFragmentByTag(tag);
            fragment.stopVideo();
        }
    }

    public void restartVideo(){
        ArrayList<StoryModel> stories = person.getStories();
        StoryModel story = stories.get(person.getCurrentStoryPos());
        if(story.getStoryType() == StoryType.VIDEO){
            String tag = String.valueOf(story.getId());
            VideoStoryFragment fragment = (VideoStoryFragment)getChildFragmentManager().findFragmentByTag(tag);
            VideoStoryModel storyVideo = (VideoStoryModel)story;
            storyVideo.setStartEnabled(true);
            fragment.restartVideo();
        }
    }

    public void onVideoReady(StoryModel story, int videoTotalSeconds){
        adapterStoryProgress.videoStarted(story, videoTotalSeconds);
    }

    public void onVideoFinish(){
        goNextStory();
    }

    public void goPreviousStory(){
        if(person.getCurrentStoryPos()-1 == -1){
            adapterStoryProgress.cancelTimers();
            activity.goPreviousPerson();
            return;
        }
        person.setCurrentStoryPos(person.getCurrentStoryPos()-1);
        getChildFragmentManager().popBackStackImmediate();
        notifyDataSetChanged();// update timer
    }

    public void goNextStory(){
        ArrayList<StoryModel> stories = person.getStories();
        if(person.getCurrentStoryPos()+1 == stories.size()){
            //no more stories for person go to next person
            adapterStoryProgress.cancelTimers();
            activity.goNextPerson();
            return;
        }
        person.setCurrentStoryPos(person.getCurrentStoryPos()+1);
        updateUIForNextStory();
    }

    public void replaceFragment(Fragment fragment, int id) {
        FragmentTransaction transaction = childFragmentManger.beginTransaction();
        transaction.replace(R.id.storyContainer, fragment, String.valueOf(id));
        if(person.getCurrentStoryPos() != 0){
            transaction.addToBackStack(null);
        }
        transaction.commitAllowingStateLoss();
    }

    private void pauseVideo(){
        StoryModel story = person.getStories().get(person.getCurrentStoryPos());
        Fragment fragment = (Fragment)childFragmentManger.findFragmentByTag(String.valueOf(story.getId()));
        if(fragment instanceof VideoStoryFragment){
            VideoStoryFragment videoStoryFragment = (VideoStoryFragment)fragment;
            videoStoryFragment.pause();
        }
    }

    private void resumeVideo(){
        StoryModel story = person.getStories().get(person.getCurrentStoryPos());
        Fragment fragment = (Fragment)childFragmentManger.findFragmentByTag(String.valueOf(story.getId()));
        if(fragment instanceof VideoStoryFragment){
            VideoStoryFragment videoStoryFragment = (VideoStoryFragment)fragment;
            videoStoryFragment.resume();
        }
    }

    private View.OnTouchListener OnScreenTouch(){
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                long duration = event.getEventTime() - event.getDownTime();

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    adapterStoryProgress.pause();
                    pauseVideo();
                }

                if(event.getAction() == MotionEvent.ACTION_UP){
                    if(duration<CLICK_THRESHOLD){
                        OnScreenClick(v);
                        adapterStoryProgress.click();
                    }
                    else {
                        adapterStoryProgress.resume();
                        resumeVideo();
                    }

                }
                return true;
            }
        };
    }

    private void OnScreenClick(View v){
        int areaId = (int)v.getTag();
        if(areaId == 0){
            goPreviousStory();//left click
            return;
        }
        goNextStory();//right click
    }

    @Override
    public void onDestroy() {
        if(adapterStoryProgress != null){
            adapterStoryProgress.cancelTimers();
        }
        super.onDestroy();
    }
}
