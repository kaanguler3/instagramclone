package com.example.codeway.Fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.palette.graphics.Palette;

import com.example.codeway.Constants;
import com.example.codeway.Model.Story.ImageStoryModel;
import com.example.codeway.R;
import com.example.codeway.Model.Story.StoryModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class ImageStoryFragment extends Fragment {
    private StoryContainerFragment parentFragment;
    private ImageStoryModel story;
    private ImageView imageStory;
    private View pnlMain;
    private GradientDrawable gradientDrawable;

    public static ImageStoryFragment newInstance(StoryModel story) {
        Bundle args = new Bundle();
        ImageStoryFragment fragment = new ImageStoryFragment();
        args.putSerializable(Constants.KEY_STORY, story);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        story = (ImageStoryModel) getArguments().getSerializable(Constants.KEY_STORY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_story, container, false);
        pnlMain = view.findViewById(R.id.pnlMain);
        imageStory = view.findViewById(R.id.imageStory);

        initializeUI();
        return view;
    }

    private void initializeUI(){
        Picasso.get().load(story.getUrl()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                imageStory.setImageBitmap(bitmap);
                if(gradientDrawable != null){
                    pnlMain.setBackground(gradientDrawable);
                    return;
                }
                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette palette) {
                        int muted = palette.getMutedColor(0x000000);
                        int mutedLight = palette.getLightMutedColor(0x000000);
                        int mutedDark = palette.getDarkMutedColor(0x000000);
                        gradientDrawable = new GradientDrawable(
                                GradientDrawable.Orientation.TOP_BOTTOM,
                                new int[] {muted, mutedLight, mutedDark});
                        gradientDrawable.setCornerRadius(0f);
                        pnlMain.setBackground(gradientDrawable);

                    }
                });
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }
}
