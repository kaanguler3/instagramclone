package com.example.codeway.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.ToxicBakery.viewpager.transforms.CubeOutTransformer;
import com.example.codeway.Adapter.StoryContainerAdapter;
import com.example.codeway.Constants;
import com.example.codeway.Model.PersonModel;
import com.example.codeway.Model.Story.StoryModel;
import com.example.codeway.R;
import com.example.codeway.ViewPagerCustomDuration;

import java.util.ArrayList;

public class StoriesActivity extends AppCompatActivity {
    private ViewPagerCustomDuration viewPager;
    private StoryContainerAdapter adapter;
    private ArrayList<PersonModel> people;
    private int clickedPersonPos;
    private int currentPersonPos;
    private int previousPersonPos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setPageTransformer(true, new CubeOutTransformer());
        viewPager.setOffscreenPageLimit(3); //memory will keep 3 containers fragment
        viewPager.setScrollDuration(300);
        initializeUI();
    }

    private void initializeUI(){
        people = (ArrayList<PersonModel>) getIntent().getSerializableExtra(Constants.KEY_PEOPLE);
        clickedPersonPos = getIntent().getIntExtra(Constants.KEY_SELECTED_PERSON_POSITION, -1);
        currentPersonPos = clickedPersonPos;
        previousPersonPos = currentPersonPos;

        //user will see stories selected person and people after
        ArrayList<PersonModel> allItems = new ArrayList<>();
        allItems.addAll(people.subList(clickedPersonPos, people.size()));
        //set adapter
        adapter = new StoryContainerAdapter(getSupportFragmentManager(), allItems);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(OnPageChange());
    }

    public void goPreviousPerson(){
        updateCurrentPersonPos(viewPager.getCurrentItem());
        if(currentPersonPos == 0){
            //there is no previous person
            onBackPressed();
            return;
        }

        //go to prev person if exist
        updateCurrentPersonPos(currentPersonPos-1);
        setCurrentItem(currentPersonPos);
    }

    public void goNextPerson(){
        currentPersonPos = viewPager.getCurrentItem();
        if(currentPersonPos == adapter.getCount()-1){
            //no person left
            onBackPressed();
            return;
        }

        updateCurrentPersonPos(currentPersonPos+1);
        setCurrentItem(currentPersonPos);
    }

    public void setStoryWatched(PersonModel person, int storyPos){
        int i=0;
        while (i<people.size()){
            PersonModel item = people.get(i);
            if(item.getId() == person.getId()){
                StoryModel story = item.getStories().get(storyPos);
                story.setWatched(true);
                return;
            }
            i++;
        }
    }

    private void updateCurrentPersonPos(int currentPersonPos){
        this.currentPersonPos = currentPersonPos;
    }

    public void setCurrentItem(int position){
        viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(position, true);
            }
        }, 10);
    }

    public PersonModel getCurrentPerson() {
        return people.get(currentPersonPos);
    }

    private ViewPager.OnPageChangeListener OnPageChange(){
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                PersonModel person = people.get(clickedPersonPos);
                setStoryWatched(person, person.getCurrentStoryPos());
                updateCurrentPersonPos(clickedPersonPos+position);
                adapter.updateProgressView(previousPersonPos, position);
                previousPersonPos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra(Constants.KEY_PEOPLE, people);
        setResult(RESULT_OK, intent);
        finish();
    }
}