package com.example.codeway.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.example.codeway.Adapter.PeopleStoriesRecyclerAdapter;
import com.example.codeway.Constants;
import com.example.codeway.Data.PersonDataFactory;
import com.example.codeway.Enum.StoryType;
import com.example.codeway.Interface.RecyclerItemClickListener;
import com.example.codeway.Model.PersonModel;
import com.example.codeway.Model.Story.StoryModel;
import com.example.codeway.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    PeopleStoriesRecyclerAdapter adapterPeople;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        initializeUI();
        cacheImages();
    }

    private void initializeUI(){
        ArrayList<PersonModel> people = PersonDataFactory.generateData();
        adapterPeople = new PeopleStoriesRecyclerAdapter(MainActivity.this, people, OnPersonItemClick());
        recyclerView.setAdapter(adapterPeople);
    }

    private void cacheImages(){
        for (PersonModel person: adapterPeople.getAllItems()) {
            for (StoryModel story: person.getStories()) {
                if(story.getStoryType() == StoryType.IMAGE){
                    Picasso.get().load(story.getUrl()).fetch();
                }
            }
        }
    }

    private RecyclerItemClickListener OnPersonItemClick(){
        return new RecyclerItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                PersonModel item = adapterPeople.getItem(position);
                Intent intent = new Intent(MainActivity.this, StoriesActivity.class);
                intent.putExtra(Constants.KEY_PEOPLE, adapterPeople.getAllItems());
                intent.putExtra(Constants.KEY_SELECTED_PERSON_POSITION, position);
                startActivityForResult(intent, Constants.RESULT_CODE_PEOPLE); //will get updated data on activity result from StoriesActivity
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == Constants.RESULT_CODE_PEOPLE){
            ArrayList<PersonModel> updatedPeople = (ArrayList<PersonModel>) data.getSerializableExtra(Constants.KEY_PEOPLE);
            for (int i=0; i<adapterPeople.getAllItems().size(); i++) {
                PersonModel person = adapterPeople.getItem(i);
                person.setStories(updatedPeople.get(i).getStories());
            }
            adapterPeople.notifyDataSetChanged();
        }
    }
}