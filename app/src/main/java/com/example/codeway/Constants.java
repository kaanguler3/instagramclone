package com.example.codeway;

public class Constants {
    public static final String KEY_STORY = "KEY_STORY";
    public static final String KEY_PEOPLE = "KEY_PEOPLE";
    public static final String KEY_PERSON = "KEY_PERSON";
    public static final String KEY_SELECTED_PERSON_POSITION = "KEY_SELECTED_PERSON_POSITION";
    public static final int RESULT_CODE_PEOPLE = 1;
    public static final int DURATION_IMAGE_STORY_SECONDS = 5;
}
