package com.example.codeway.Interface;

import android.view.View;

public interface RecyclerItemClickListener {
    void onItemClick(View view, int position);
}
